@extends('layouts.global')
@section('title')
Create Categori
@endsection
@section('content')
<div class="col-md-8">
    @if(session('status'))
    <div class="alert alert-success">
        {{session('status')}}
    </div>
    @endif
    <form action="{{ route('categories.store') }} " method="POST" enctype="multipart/form-data"
        class=" bg-white shadow-sm p-2">
        @csrf
        <div class="form-group">
            <label for="name">Category Name</label>
            <input class="form-control" type="text" name="name" id="name">
        </div>
        <div class="custom-file">
            <input type="file" class="custom-file-input" id="image" name="image">
            <label class="custom-file-label" for="image">Choose Image</label>
        </div>
        <input type="submit" value="Save" class="btn btn-primary mt-2">
    </form>
</div>
@endsection
