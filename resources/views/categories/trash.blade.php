@extends('layouts.global')
@section('title')
Category List
@endsection
@section('content')
@if(session('status'))
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-warning">
            {{session('status')}}
        </div>
    </div>
</div>
@endif
<div class="row mb-2">
    <div class="col-6">
        <form action="{{ route('categories.index') }} ">
            <div class="input-group">
                <input type="text" class="form-control" aria-describedby="basic-addon2" name="keyword"
                    value="{{Request::get('keyword')}}" placeholder="Filter Berdasarkan Nama Barang">
                <div class="input-group-append">
                    <button class="btn btn-primary" type="submit">Filter</button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-6">
        <ul class="nav nav-pills">
            <li class="nav-item">
                <a class="nav-link" href="{{route('categories.index')}}">Published</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="{{route('categories.trash')}}">Trash</a>
            </li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive-md">
            <table class="table table-bordered table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Slug</th>
                        <th scope="col">Image</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($categories_delete as $all_categories)
                    <tr>
                        {{-- <th scope="row">{{ $a++ }} </th> --}}
                        <th scope="row">{{ $all_categories->name }} </th>
                        <td>{{ $all_categories->slug }} </td>
                        <td>
                            @if($all_categories->image)
                            <img src="{{asset('storage/'.$all_categories->image)}}" width="70px" height="50px" />
                            @else
                            No Image
                            @endif
                        </td>
                        <td>
                            <a href="{{route('categories.restore',[$all_categories->id])}}"
                                class="btn btn-success btn-sm "><span class="fa fa-refresh fa-lg"></span>
                            </a>
                            <form action="{{route('categories.deletepermanent',[$all_categories->id])}}" method="POST"
                                onsubmit="return confirm('Delete this Category permanently?')" class="d-inline">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">
                                <button type="submit" class="btn btn-danger btn-sm"><span
                                        class="fa fa-trash fa-lg"></span></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan=10>
                            {{$categories_delete->appends(Request::all())->links()}}
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

@endsection
