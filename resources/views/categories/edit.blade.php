@extends('layouts.global')
@section('title')
Edit Category
@endsection
@section('content')
<div class="col-md-8">
    @if(session('status'))
    <div class="alert alert-success">
        {{session('status')}}
    </div>
    @endif
    <form action="{{route('categories.update',[$categories->id])}} " method="post" enctype="multipart/form-data"
        class="bg-white p-3 shadow">
        @csrf
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
            <label for="name">Category Name</label>
            <input class="form-control" type="text" name="name" id="name" value="{{$categories->name}}">
        </div>
        <div class="form-group">
            <label for="slug">Category Slug</label>
            <input class="form-control" type="text" name="slug" id="slug" value="{{$categories->slug}}">
        </div>
        @if ($categories->image)
        <span>Curent Image</span><br>
        <img src="{{asset('storage/'.$categories->image)}}" width="120px" alt="">
        @endif
        <div class="custom-file mt-2">
            <input type="file" class="custom-file-input" id="image" name="image">
            <label class="custom-file-label" for="image">Choose Image</label>
            <small>Kosongkan jika tidak ingin memngganti gambar</small>
        </div>
        <input type="submit" value="Save" class="btn btn-primary mt-1">
    </form>
</div>
@endsection
