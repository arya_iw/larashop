@extends('layouts.global')
@section('title')
All User
@endsection
@section('content')
@if(session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
@endif
<form action="{{route('users.index')}} ">
    <div class="row">
        <div class="col-5">
            <div class="form-group">
                <input class="form-control" type="text" name="keyword" value="{{Request::get('keyword')}}"
                    placeholder="Masukan email untuk filter">
            </div>
        </div>
        <div class="col-7 pt-1">
            <div class="form-check form-check-inline">
                <input {{Request::get('status')=='ACTIVE'?'checked':''}} class="form-check-input" type="radio"
                    name="status" id="ACTIVE" value="ACTIVE">
                <label class="form-check-label" for="ACTIVE">Active</label>
            </div>
            <div class="form-check form-check-inline">
                <input {{Request::get('status')=='INACTIVE'?'checked':''}} class="form-check-input" type="radio"
                    name="status" id="INACTIVE" value="INACTIVE">
                <label class="form-check-label" for="INACTIVE">Inactive</label>
                <button type="submit" class="btn btn-primary">filter</button>
            </div>
        </div>
    </div>
</form>
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive-md">
            <table class="table table-bordered table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Username</th>
                        <th scope="col">Email</th>
                        <th scope="col">Avatar</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($all_user as $user)
                    <tr>
                        {{-- <th scope="row">{{ $a++ }} </th> --}}
                        <th scope="row">{{ $user->name }} </th>
                        <td>{{$user->username}} </td>
                        <td>{{$user->email}} </td>
                        <td>
                            @if($user->avatar)
                            <img src="{{asset('storage/'.$user->avatar)}}" width="70px" height="50px" />
                            @else
                            N/A
                            @endif
                        </td>
                        <td>
                            @if($user->status == "ACTIVE")
                            <span class="badge badge-success">
                                {{$user->status}}
                            </span>
                            @else
                            <span class="badge badge-danger">
                                {{$user->status}}
                            </span>
                            @endif
                        </td>
                        <td><a class="btn btn-info text-white btn-sm mb-lg-auto mb-xl-auto mb-md-1"
                                href="{{route('users.edit',[$user->id])}}"><i class="oi oi-pencil"></i></a>
                            <a class="btn btn-sm btn-primary mt-lg-auto mt-md-auto mt-sm-1 mt-xl-auto mb-lg-auto mb-xl-auto mb-md-1"
                                href="{{ route('users.show',[$user->id])}} "><i class="fa fa-info-circle fa-lg"></i></a>
                            <form onsubmit="return confirm('Delete this user permanently?')" class="d-inline"
                                action="{{route('users.destroy', [$user->id])}}" method="POST">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">
                                <button type="submit"
                                    class="btn btn-danger btn-sm mt-lg-auto mt-md-auto mt-sm-1 mt-xl-auto"><i
                                        class="fa fa-trash fa-lg"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan=10>
                            {{$all_user->appends(Request::all())->links()}}
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
