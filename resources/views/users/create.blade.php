@extends("layouts.global")
@section("title") Create User @endsection
@section("content")
<div class="col-md-8">
    @if(session('status'))
    <div class="alert alert-success">
        {{session('status')}}
    </div>
    @endif
    <form enctype="multipart/form-data" class="bg-white shadow-sm p-3" action="{{route('users.store')}}" method="POST">
        @csrf
        <div class="form-group">
            <label for="name">Nama</label>
            <input class="form-control" type="text" name="name" id="name" placeholder="Fullname">
        </div>
        <div class="form-group">
            <label for="username">Username</label>
            <input class="form-control" type="text" name="username" id="username">
            <small class="form-text text-muted">Name in your dashboard</small>
        </div>
        <label for="">Roles</label><br>
        <div class=" form-check form-check-inline ml-3">
            <input class="form-check-input" type="checkbox" value="ADMIN" name="roles[]" id="ADMIN">
            <label class="form-check-label" for="ADMIN">Administrator</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" value="STAFF" name="roles[]" id="STAFF">
            <label class="form-check-label" for="STAFF">Staff</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" value="CUSTOMER" name="roles[]" id="CUSTOMER">
            <label class="form-check-label" for="CUSTOMER">Customer</label>
        </div>
        <div class="form-group mt-2">
            <label for="phone">Phone number</label>
            <input class="form-control" type="text" name="phone" id="phone">
            <small class="form-text text-muted">Nomer Handphone Anda</small>
        </div>
        <div class="form-group">
            <label for="address">Address</label>
            <textarea id="address" class="form-control" rows="2" name="address"></textarea>
        </div>
        <div class="custom-file">
            <input type="file" class="custom-file-input" id="avatar" name="avatar">
            <label class="custom-file-label" for="avatar">Avatar Image</label>
        </div>
        <hr class="my-3">
        <div class="form-group">
            <label for="email">Email</label>
            <input class="form-control" type="email" name="email" id="email" placeholder="user@mail.com">
            <small class="form-text text-muted"></small>
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input class="form-control" type="password" name="password" id="password" placeholder="password">
            <small class="form-text text-muted"></small>
        </div>
        <div class="form-group">
            <label for="password_confirmation">Password Confirmation</label>
            <input class="form-control" type="password" name="password_confirmation" id="password_confirmation"
                placeholder="password confirmation">
            <small class="form-text text-muted"></small>
        </div>
        <input class="btn btn-primary" type="submit" value="Save">
    </form>
</div>
@endsection
