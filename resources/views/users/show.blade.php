@extends('layouts.global')
@section('title')
Detail USer
@endsection
@section('content')
<div class="col-md-8 bg-white shadow rounded">
    <div class="row">
        <div class="col-md-3 text-center bg-primary-darker p-4 ">
            <section style="min-height: 150px">
                @if ($user->avatar)
                <img class="img-fluid rounded-circle" src="{{asset('storage/'. $user->avatar)}}" width="150px" />
                @else
                No Avatar
                @endif
            </section>
            <div class="mt-3">
                <a class="btn btn-info  btn-sm " href="{{route('users.edit',[$user->id])}}"><i
                        class="fa fa-pencil fa-lg"></i></a>
                <form onsubmit="return confirm('Delete this user permanently?')" class="d-inline"
                    action="{{route('users.destroy', [$user->id])}}" method="POST">
                    @csrf
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash fa-lg"></i></button>
                </form>
            </div>
        </div>
        <div class="col-md-9">
            <div class="mt-4 mt-lg-2 mt-xl-2 mb-lg-1 mb-xl-1">
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span style="width:100px;" class="input-group-text font-weight-bold"
                            id="inputGroup-sizing-default">Name</span>
                    </div>
                    <input type="text" class="form-control" aria-label="Default"
                        aria-describedby="inputGroup-sizing-default" value="{{$user->name}}" disabled>
                </div>
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span style="width:100px;" class="input-group-text font-weight-bold"
                            id="inputGroup-sizing-default">Email</span>
                    </div>
                    <input type="text" class="form-control" aria-label="Default"
                        aria-describedby="inputGroup-sizing-default" value="{{$user->email}}" disabled>
                </div>
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span style="width:100px; " class="input-group-text font-weight-bold"
                            id="inputGroup-sizing-default">Phone</span>
                    </div>
                    <input type="text" class="form-control" aria-label="Default"
                        aria-describedby="inputGroup-sizing-default" value="{{$user->phone}}" disabled>
                </div>
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span style="width:100px;" class="input-group-text font-weight-bold"
                            id="inputGroup-sizing-default">Address</span>
                    </div>
                    <input type="text" class="form-control" aria-label="Default"
                        aria-describedby="inputGroup-sizing-default" value="{{$user->address}}" disabled>
                </div>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <span style="width:100px;" class="input-group-text font-weight-bold">Roles</span>
                    </div>
                    <textarea class="form-control" aria-label="With textarea" rows="3" disabled
                        style="overflow:hidden;resize:none;">@foreach (json_decode($user->roles) as $role)&middot;{{$role}}&#13;&#10;@endforeach</textarea>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
