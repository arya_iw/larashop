<?php $__env->startSection('title'); ?>
Edit User
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="col-md-8">
    <?php if(session('status')): ?>
    <div class="alert alert-success">
        <?php echo e(session('status')); ?>

    </div>
    <?php endif; ?>
    <form enctype="multipart/form-data" class="bg-white shadow p-3" action="<?php echo e(route('users.update', [$user->id])); ?>"
        method="POST">
        <?php echo csrf_field(); ?>
        <input type="hidden" value="PUT" name="_method">
        <div class="form-group">
            <label for="name">Nama</label>
            <input class="form-control" type="text" name="name" id="name" placeholder="Fullname"
                value="<?php echo e($user->name); ?> ">
        </div>
        <div class="form-group">
            <label for="username">Username</label>
            <input class="form-control" type="text" name="username" id="username" value="<?php echo e($user->username); ?>"
                disabled>
            <small class="form-text text-muted">Name in your dashboard</small>
        </div>
        <label for="">Status</label><br>
        <div class="form-check form-check-inline ml-3">
            <input <?php echo e($user->status == "ACTIVE" ? "checked" : ""); ?> class="form-check-input" type="radio" name="status"
                id="active" value="ACTIVE">
            <label class="form-check-label" for="active">Active</label>
        </div>
        <div class="form-check form-check-inline">
            <input <?php echo e($user->status == "INACTIVE" ? "checked" : ""); ?> class="form-check-input" type="radio" name="status"
                id="inactive" value="INACTIVE">
            <label class="form-check-label" for="inactive">Inactive</label>
        </div>

        <br><label class="mt-3" for="">Roles</label><br>
        <div class=" form-check form-check-inline ml-3">
            <input <?php echo e(in_array("ADMIN", json_decode($user->roles)) ? "checked" : ""); ?> class="form-check-input"
                type="checkbox" value="ADMIN" name="roles[]" id="ADMIN">
            <label class="form-check-label" for="ADMIN">Administrator</label>
        </div>
        <div class="form-check form-check-inline">
            <input <?php echo e(in_array("STAFF", json_decode($user->roles)) ? "checked" : ""); ?> class="form-check-input"
                type="checkbox" value="STAFF" name="roles[]" id="STAFF">
            <label class="form-check-label" for="STAFF">Staff</label>
        </div>
        <div class="form-check form-check-inline">
            <input <?php echo e(in_array("CUSTOMER", json_decode($user->roles)) ? "checked" : ""); ?> class="form-check-input"
                type="checkbox" value="CUSTOMER" name="roles[]" id="CUSTOMER">
            <label class="form-check-label" for="CUSTOMER">Customer</label>
        </div>
        <div class="form-group mt-2">
            <label for="phone">Phone number</label>
            <input class="form-control" type="text" name="phone" id="phone" value="<?php echo e($user->phone); ?> ">
            <small class="form-text text-muted">Nomer Handphone Anda</small>
        </div>
        <div class="form-group">
            <label for="address">Address</label>
            <textarea id="address" class="form-control" rows="2" name="address"><?php echo e($user->address); ?> </textarea>
        </div>
        <?php if($user->avatar): ?>
        <img src="<?php echo e(asset('storage/'.$user->avatar)); ?>" width="120" /><br>
        <br>
        <?php else: ?>
        <label class="bg-danger text-white pl-1 pr-1" for="">No Avatar</label><br>
        <?php endif; ?>
        <div class="custom-file">
            <input type="file" class="custom-file-input" id="avatar" name="avatar">
            <label class="custom-file-label" for="avatar">Avatar Image</label>
            <small class="form-text text-muted">Abaikan jika tidak ingin mengganti avatar</small>
        </div>
        <hr class="my-3 font-weight-bold">
        <div class="form-group">
            <label for="email">Email</label>
            <input class="form-control" type="email" name="email" id="email" placeholder="user@mail.com"
                value="<?php echo e($user->email); ?>" disabled>
            <small class="form-text text-muted"></small>
        </div>
        <input class="btn btn-primary" type="submit" value="Save">
    </form>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.global', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ax404/Code/larashop/resources/views/users/edit.blade.php ENDPATH**/ ?>