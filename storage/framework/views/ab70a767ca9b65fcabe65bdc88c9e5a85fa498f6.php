<?php $__env->startSection('title'); ?>
All User
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php if(session('status')): ?>
<div class="alert alert-success">
    <?php echo e(session('status')); ?>

</div>
<?php endif; ?>
<form action="<?php echo e(route('users.index')); ?> ">
    <div class="row">
        <div class="col-5">
            <div class="form-group">
                <input class="form-control" type="text" name="keyword" value="<?php echo e(Request::get('keyword')); ?>"
                    placeholder="Masukan email untuk filter">
            </div>
        </div>
        <div class="col-7 pt-1">
            <div class="form-check form-check-inline">
                <input <?php echo e(Request::get('status')=='ACTIVE'?'checked':''); ?> class="form-check-input" type="radio"
                    name="status" id="ACTIVE" value="ACTIVE">
                <label class="form-check-label" for="ACTIVE">Active</label>
            </div>
            <div class="form-check form-check-inline">
                <input <?php echo e(Request::get('status')=='INACTIVE'?'checked':''); ?> class="form-check-input" type="radio"
                    name="status" id="INACTIVE" value="INACTIVE">
                <label class="form-check-label" for="INACTIVE">Inactive</label>
                <button type="submit" class="btn btn-primary">filter</button>
            </div>
        </div>
    </div>
</form>
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive-md">
            <table class="table table-bordered table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Username</th>
                        <th scope="col">Email</th>
                        <th scope="col">Avatar</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $all_user; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        
                        <th scope="row"><?php echo e($user->name); ?> </th>
                        <td><?php echo e($user->username); ?> </td>
                        <td><?php echo e($user->email); ?> </td>
                        <td>
                            <?php if($user->avatar): ?>
                            <img src="<?php echo e(asset('storage/'.$user->avatar)); ?>" width="70px" height="50px" />
                            <?php else: ?>
                            N/A
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if($user->status == "ACTIVE"): ?>
                            <span class="badge badge-success">
                                <?php echo e($user->status); ?>

                            </span>
                            <?php else: ?>
                            <span class="badge badge-danger">
                                <?php echo e($user->status); ?>

                            </span>
                            <?php endif; ?>
                        </td>
                        <td><a class="btn btn-info text-white btn-sm mb-lg-auto mb-xl-auto mb-md-1"
                                href="<?php echo e(route('users.edit',[$user->id])); ?>"><i class="oi oi-pencil"></i></a>
                            <a class="btn btn-sm btn-primary mt-lg-auto mt-md-auto mt-sm-1 mt-xl-auto mb-lg-auto mb-xl-auto mb-md-1"
                                href="<?php echo e(route('users.show',[$user->id])); ?> "><i class="fa fa-info-circle fa-lg"></i></a>
                            <form onsubmit="return confirm('Delete this user permanently?')" class="d-inline"
                                action="<?php echo e(route('users.destroy', [$user->id])); ?>" method="POST">
                                <?php echo csrf_field(); ?>
                                <input type="hidden" name="_method" value="DELETE">
                                <button type="submit"
                                    class="btn btn-danger btn-sm mt-lg-auto mt-md-auto mt-sm-1 mt-xl-auto"><i
                                        class="fa fa-trash fa-lg"></i></button>
                            </form>
                        </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan=10>
                            <?php echo e($all_user->appends(Request::all())->links()); ?>

                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.global', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ax404/Documents/larashop/resources/views/users/index.blade.php ENDPATH**/ ?>