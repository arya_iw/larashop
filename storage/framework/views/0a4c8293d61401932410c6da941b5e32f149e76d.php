<?php $__env->startSection('title'); ?>
Detail Category
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="col-md-8 bg-white shadow rounded">
    <div class="row">
        <div class="col-md-3 text-center bg-primary-darker p-4">
            <section style="min-height:150px;">
                <?php if($categories->image): ?>
                <img class="img-fluid rounded-circle" src="<?php echo e(asset('storage/'.$categories->image)); ?>" width="150px">
                <?php endif; ?>
            </section>
            <div class="mt-3">
                <a href="<?php echo e(route('categories.edit',[$categories->id])); ?>" class="btn btn-info btn-sm"><span
                        class="oi oi-pencil"></span>
                </a>
                <form action="<?php echo e(route('categories.destroy',[$categories->id])); ?>" method="POST"
                    onsubmit="return confirm('Move category to trash?')" class="d-inline">
                    <?php echo csrf_field(); ?>
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-danger btn-sm"><span class="fa fa-trash fa-lg"></span></button>
                </form>
            </div>
        </div>
        <div class="col-md-9">
            <div class="mt-4 mt-lg-2 mt-xl-2 mb-lg-1 mb-xl-1">
                
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span style="width:100px;" class="input-group-text font-weight-bold"
                            id="inputGroup-sizing-default">Name</span>
                    </div>
                    <input type="text" class="form-control" aria-label="Default"
                        aria-describedby="inputGroup-sizing-default" value="<?php echo e($categories->name); ?>" disabled>
                </div>
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span style="width:100px;" class="input-group-text font-weight-bold"
                            id="inputGroup-sizing-default">Slug</span>
                    </div>
                    <input type="text" class="form-control" aria-label="Default"
                        aria-describedby="inputGroup-sizing-default" value="<?php echo e($categories->slug); ?>" disabled>
                </div>
                
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.global', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ax404/Code/larashop/resources/views/categories/show.blade.php ENDPATH**/ ?>