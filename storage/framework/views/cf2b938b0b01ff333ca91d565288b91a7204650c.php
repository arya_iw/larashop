<?php $__env->startSection('title'); ?>
Detail USer
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="col-md-8 bg-white shadow rounded">
    <div class="row">
        <div class="col-md-3 text-center bg-primary-darker p-4 ">
            <section style="min-height: 150px">
                <?php if($user->avatar): ?>
                <img class="img-fluid rounded-circle" src="<?php echo e(asset('storage/'. $user->avatar)); ?>" width="150px" />
                <?php else: ?>
                No Avatar
                <?php endif; ?>
            </section>
            <div class="mt-3">
                <a class="btn btn-info  btn-sm " href="<?php echo e(route('users.edit',[$user->id])); ?>"><i
                        class="fa fa-pencil fa-lg"></i></a>
                <form onsubmit="return confirm('Delete this user permanently?')" class="d-inline"
                    action="<?php echo e(route('users.destroy', [$user->id])); ?>" method="POST">
                    <?php echo csrf_field(); ?>
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash fa-lg"></i></button>
                </form>
            </div>
        </div>
        <div class="col-md-9">
            <div class="mt-4 mt-lg-2 mt-xl-2 mb-lg-1 mb-xl-1">
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span style="width:100px;" class="input-group-text font-weight-bold"
                            id="inputGroup-sizing-default">Name</span>
                    </div>
                    <input type="text" class="form-control" aria-label="Default"
                        aria-describedby="inputGroup-sizing-default" value="<?php echo e($user->name); ?>" disabled>
                </div>
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span style="width:100px;" class="input-group-text font-weight-bold"
                            id="inputGroup-sizing-default">Email</span>
                    </div>
                    <input type="text" class="form-control" aria-label="Default"
                        aria-describedby="inputGroup-sizing-default" value="<?php echo e($user->email); ?>" disabled>
                </div>
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span style="width:100px; " class="input-group-text font-weight-bold"
                            id="inputGroup-sizing-default">Phone</span>
                    </div>
                    <input type="text" class="form-control" aria-label="Default"
                        aria-describedby="inputGroup-sizing-default" value="<?php echo e($user->phone); ?>" disabled>
                </div>
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span style="width:100px;" class="input-group-text font-weight-bold"
                            id="inputGroup-sizing-default">Address</span>
                    </div>
                    <input type="text" class="form-control" aria-label="Default"
                        aria-describedby="inputGroup-sizing-default" value="<?php echo e($user->address); ?>" disabled>
                </div>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <span style="width:100px;" class="input-group-text font-weight-bold">Roles</span>
                    </div>
                    <textarea class="form-control" aria-label="With textarea" rows="3" disabled
                        style="overflow:hidden;resize:none;"><?php $__currentLoopData = json_decode($user->roles); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>&middot;<?php echo e($role); ?>&#13;&#10;<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></textarea>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.global', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ax404/Code/larashop/resources/views/users/show.blade.php ENDPATH**/ ?>