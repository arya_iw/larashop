<?php $__env->startSection('title'); ?>
Edit Category
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="col-md-8">
    <?php if(session('status')): ?>
    <div class="alert alert-success">
        <?php echo e(session('status')); ?>

    </div>
    <?php endif; ?>
    <form action="<?php echo e(route('categories.update',[$categories->id])); ?> " method="post" enctype="multipart/form-data"
        class="bg-white p-3 shadow">
        <?php echo csrf_field(); ?>
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
            <label for="name">Category Name</label>
            <input class="form-control" type="text" name="name" id="name" value="<?php echo e($categories->name); ?>">
        </div>
        <div class="form-group">
            <label for="slug">Category Slug</label>
            <input class="form-control" type="text" name="slug" id="slug" value="<?php echo e($categories->slug); ?>">
        </div>
        <?php if($categories->image): ?>
        <span>Curent Image</span><br>
        <img src="<?php echo e(asset('storage/'.$categories->image)); ?>" width="120px" alt="">
        <?php endif; ?>
        <div class="custom-file mt-2">
            <input type="file" class="custom-file-input" id="image" name="image">
            <label class="custom-file-label" for="image">Choose Image</label>
            <small>Kosongkan jika tidak ingin memngganti gambar</small>
        </div>
        <input type="submit" value="Save" class="btn btn-primary mt-1">
    </form>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.global', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ax404/Code/larashop/resources/views/categories/edit.blade.php ENDPATH**/ ?>