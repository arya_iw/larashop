<?php $__env->startSection('title'); ?>
Category List
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php if(session('status')): ?>
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-warning">
            <?php echo e(session('status')); ?>

        </div>
    </div>
</div>
<?php endif; ?>
<div class="row mb-2">
    <div class="col-6">
        <form action="<?php echo e(route('categories.index')); ?> ">
            <div class="input-group">
                <input type="text" class="form-control" aria-describedby="basic-addon2" name="keyword"
                    value="<?php echo e(Request::get('keyword')); ?>" placeholder="Filter Berdasarkan Nama Barang">
                <div class="input-group-append">
                    <button class="btn btn-primary" type="submit">Filter</button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-6">
        <ul class="nav nav-pills">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo e(route('categories.index')); ?>">Published</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="<?php echo e(route('categories.trash')); ?>">Trash</a>
            </li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive-md">
            <table class="table table-bordered table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Slug</th>
                        <th scope="col">Image</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $categories_delete; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $all_categories): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        
                        <th scope="row"><?php echo e($all_categories->name); ?> </th>
                        <td><?php echo e($all_categories->slug); ?> </td>
                        <td>
                            <?php if($all_categories->image): ?>
                            <img src="<?php echo e(asset('storage/'.$all_categories->image)); ?>" width="70px" height="50px" />
                            <?php else: ?>
                            No Image
                            <?php endif; ?>
                        </td>
                        <td>
                            <a href="<?php echo e(route('categories.restore',[$all_categories->id])); ?>"
                                class="btn btn-success btn-sm "><span class="fa fa-refresh fa-lg"></span>
                            </a>
                            <form action="<?php echo e(route('categories.deletepermanent',[$all_categories->id])); ?>" method="POST"
                                onsubmit="return confirm('Delete this Category permanently?')" class="d-inline">
                                <?php echo csrf_field(); ?>
                                <input type="hidden" name="_method" value="DELETE">
                                <button type="submit" class="btn btn-danger btn-sm"><span
                                        class="fa fa-trash fa-lg"></span></button>
                            </form>
                        </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan=10>
                            <?php echo e($categories_delete->appends(Request::all())->links()); ?>

                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.global', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ax404/Code/larashop/resources/views/categories/trash.blade.php ENDPATH**/ ?>