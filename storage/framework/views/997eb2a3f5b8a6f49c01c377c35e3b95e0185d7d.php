<?php $__env->startSection('title'); ?>
Create Categori
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="col-md-8">
    <?php if(session('status')): ?>
    <div class="alert alert-success">
        <?php echo e(session('status')); ?>

    </div>
    <?php endif; ?>
    <form action="<?php echo e(route('categories.store')); ?> " method="POST" enctype="multipart/form-data"
        class=" bg-white shadow-sm p-2">
        <?php echo csrf_field(); ?>
        <div class="form-group">
            <label for="name">Category Name</label>
            <input class="form-control" type="text" name="name" id="name">
        </div>
        <div class="custom-file">
            <input type="file" class="custom-file-input" id="image" name="image">
            <label class="custom-file-label" for="image">Choose Image</label>
        </div>
        <input type="submit" value="Save" class="btn btn-primary mt-2">
    </form>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.global', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ax404/Documents/larashop/resources/views/categories/create.blade.php ENDPATH**/ ?>