<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::match(["GET", "POST"], "/register", function () {
    return redirect("/login");
})->name("register");

// Router web larashop
Route::resource('users', 'UsersController');
Route::get('categories/trash', 'CategoryController@trash')->name('categories.trash');
Route::get('categories/{id}/restore', 'CategoryController@restore')->name('categories.restore');
Route::delete('categories/{id}/delete-permanent', 'CategoryController@deletepermanent')->name('categories.deletepermanent');
Route::resource('categories', 'CategoryController');
